AUI.add(
	'email-field',
	function(A) {
		var EmailField = A.Component.create(
			{
				ATTRS: {
					email: {
						value: ''
					},
					prefill: {}
				},

				EXTENDS: Liferay.DDM.Renderer.Field,

				NAME: 'email-field',

				prototype: {
					getTemplateContext: function() {
						var instance = this;

						return A.merge(
							EmailField.superclass.getTemplateContext.apply(instance, arguments),
							{
								email: renderEmail(instance)
							}
						);
					}
				}

			}
		);

		Liferay.namespace('DDM.Field').EmailField = EmailField;
	},
	'',
	{
		requires: ['liferay-ddm-form-renderer-field']
	}
);

function renderEmail(instance) {
	return instance.get('prefill') ? fetchEmail() : '';
}

function fetchEmail() {
	var xmlHttp = new XMLHttpRequest();
	var url = "http://localhost:9080/o/custom-forms/forms/" + getLiferayUserId() + "/email";
	xmlHttp.open( "GET", url, false );
	xmlHttp.send( null );

	return xmlHttp.responseText
}


function getLiferayUserId() {
	return Liferay.ThemeDisplay.getUserId();
}