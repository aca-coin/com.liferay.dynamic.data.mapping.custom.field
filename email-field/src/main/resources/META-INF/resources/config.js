;(function() {
	AUI().applyConfig(
		{
			groups: {
				'email-group': {
					base: MODULE_PATH + '/',
					combine: Liferay.AUI.getCombine(),
					modules: {
						'email-field': {
							condition: {
								trigger: 'liferay-ddm-form-renderer'
							},
							path: 'email-field.js',
							requires: [
								'liferay-ddm-form-renderer-field'
							]
						},
						'email-field-template': {
							condition: {
								trigger: 'liferay-ddm-form-renderer'
							},
							path: 'email-field.soy.js',
							requires: [
								'soyutils'
							]
						}
					},
					root: MODULE_PATH + '/'
				}
			}
		}
	);
})();