package com.liferay.docs.form.field;

import com.liferay.dynamic.data.mapping.annotations.*;
import com.liferay.dynamic.data.mapping.form.field.type.DefaultDDMFormFieldTypeSettings;

@DDMForm
@DDMFormLayout(
		paginationMode = com.liferay.dynamic.data.mapping.model.DDMFormLayout.TABBED_MODE,
		value = {
				@DDMFormLayoutPage(
						title = "%basic",
						value = {
								@DDMFormLayoutRow(
										{
												@DDMFormLayoutColumn(
														size = 12,
														value = {
																"label", "required", "tip", "prefill"
														}
												)
										}
								)
						}
				),
				@DDMFormLayoutPage(
						title = "%properties",
						value = {
								@DDMFormLayoutRow(
										{
												@DDMFormLayoutColumn(
														size = 12,
														value = {
																"dataType", "name", "showLabel", "repeatable",
																"type", "validation", "visibilityExpression"
														}
												)
										}
								)
						}
				)
		}
)
public interface EmailFieldTypeSettings extends DefaultDDMFormFieldTypeSettings {

	@DDMFormField(label = "%prefill")
	boolean prefill();
}
