package com.liferay.docs.form.field;

import org.osgi.service.component.annotations.Component;

import com.liferay.dynamic.data.mapping.form.field.type.BaseDDMFormFieldType;
import com.liferay.dynamic.data.mapping.form.field.type.DDMFormFieldType;
import com.liferay.dynamic.data.mapping.form.field.type.DDMFormFieldTypeSettings;

@Component(
		immediate = true,
		property = {
				"ddm.form.field.type.display.order:Integer=14",
				"ddm.form.field.type.icon=text",
				"ddm.form.field.type.js.class.name=Liferay.DDM.Field.EmailField",
				"ddm.form.field.type.js.module=email-field",
				"ddm.form.field.type.label=email-field-label",
				"ddm.form.field.type.name=EmailField"
		},
		service = DDMFormFieldType.class
)
public class EmailField extends BaseDDMFormFieldType {

	@Override
	public String getName() {
		return "EmailField";
	}

	@Override
	public Class<? extends DDMFormFieldTypeSettings> getDDMFormFieldTypeSettings() {
		return EmailFieldTypeSettings.class;
	}
}
