package com.liferay.docs.form.field;

import java.util.Map;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;

import com.liferay.dynamic.data.mapping.form.field.type.BaseDDMFormFieldRenderer;
import com.liferay.dynamic.data.mapping.form.field.type.DDMFormFieldRenderer;
import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.render.DDMFormFieldRenderingContext;
import com.liferay.portal.kernel.template.Template;
import com.liferay.portal.kernel.template.TemplateConstants;
import com.liferay.portal.kernel.template.TemplateResource;

@Component(
		immediate = true, property = "ddm.form.field.type.name=EmailField",
		service = DDMFormFieldRenderer.class
)
public class EmailFieldRenderer extends BaseDDMFormFieldRenderer {

	private TemplateResource _templateResource;

	@Activate
	protected void activate(Map<String, Object> properties) {
		_templateResource = getTemplateResource(
				"/META-INF/resources/email-field.soy");
	}

	@Override
	public String getTemplateLanguage() {
		return TemplateConstants.LANG_TYPE_SOY;
	}

	@Override
	public String getTemplateNamespace() {
		return "ddm.EmailField";
	}

	@Override
	public TemplateResource getTemplateResource() {
		return _templateResource;
	}

	@Override
	protected void populateOptionalContext(Template template, DDMFormField ddmFormField, DDMFormFieldRenderingContext ddmFormFieldRenderingContext) {
		template.put("email", "");
	}
}
