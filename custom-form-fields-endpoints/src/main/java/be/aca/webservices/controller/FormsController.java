package be.aca.webservices.controller;

import javax.ws.rs.*;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;


import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalService;

@ApplicationPath("/forms")
@Component(immediate = true, service = Application.class, property = { "jaxrs.application=true" })
public class FormsController extends Application {

	@Reference
	UserLocalService userLocalService;

	@GET
	@Path("/{userId}/email")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getArticleContent(@PathParam("userId") long userId) throws Exception {
		User user = userLocalService.getUser(userId);

		return Response.ok(user.getEmailAddress()).build();
	}
}
