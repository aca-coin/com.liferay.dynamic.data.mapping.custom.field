package com.liferay.docs.form.field;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;

import com.liferay.dynamic.data.mapping.form.field.type.BaseDDMFormFieldRenderer;
import com.liferay.dynamic.data.mapping.form.field.type.DDMFormFieldRenderer;
import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.render.DDMFormFieldRenderingContext;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.template.Template;
import com.liferay.portal.kernel.template.TemplateConstants;
import com.liferay.portal.kernel.template.TemplateResource;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;

@Component(
		immediate = true, property = "ddm.form.field.type.name=NameField",
		service = DDMFormFieldRenderer.class
)
public class NameFieldRenderer extends BaseDDMFormFieldRenderer {

	private TemplateResource _templateResource;

	@Activate
	protected void activate(Map<String, Object> properties) {
		_templateResource = getTemplateResource(
				"/META-INF/resources/name-field.soy");
	}

	@Override
	public String getTemplateLanguage() {
		return TemplateConstants.LANG_TYPE_SOY;
	}

	@Override
	public String getTemplateNamespace() {
		return "ddm.NameField";
	}

	@Override
	public TemplateResource getTemplateResource() {
		return _templateResource;
	}

	@Override
	protected void populateOptionalContext(Template template, DDMFormField ddmFormField, DDMFormFieldRenderingContext ddmFormFieldRenderingContext) {
		HttpServletRequest httpServletRequest = ddmFormFieldRenderingContext.getHttpServletRequest();
		ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);
		User user = themeDisplay.getUser();

		template.put("username", user.getFullName());
	}
}
