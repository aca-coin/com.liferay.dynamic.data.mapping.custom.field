package com.liferay.docs.form.field;

import org.osgi.service.component.annotations.Component;

import com.liferay.dynamic.data.mapping.form.field.type.BaseDDMFormFieldType;
import com.liferay.dynamic.data.mapping.form.field.type.DDMFormFieldType;
import com.liferay.dynamic.data.mapping.form.field.type.DDMFormFieldTypeSettings;

@Component(
		immediate = true,
		property = {
				"ddm.form.field.type.display.order:Integer=14",
				"ddm.form.field.type.icon=text",
				"ddm.form.field.type.js.class.name=Liferay.DDM.Field.NameField",
				"ddm.form.field.type.js.module=name-field",
				"ddm.form.field.type.label=name-field-label",
				"ddm.form.field.type.name=NameField"
		},
		service = DDMFormFieldType.class
)
public class NameField extends BaseDDMFormFieldType {

	@Override
	public String getName() {
		return "NameField";
	}
}