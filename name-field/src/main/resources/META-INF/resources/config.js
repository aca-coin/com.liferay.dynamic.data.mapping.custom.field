;(function() {
	AUI().applyConfig(
		{
			groups: {
				'name-group': {
					base: MODULE_PATH + '/',
					combine: Liferay.AUI.getCombine(),
					modules: {
						'name-field': {
							condition: {
								trigger: 'liferay-ddm-form-renderer'
							},
							path: 'name-field.js',
							requires: [
								'liferay-ddm-form-renderer-field'
							]
						},
						'name-field-template': {
							condition: {
								trigger: 'liferay-ddm-form-renderer'
							},
							path: 'name-field.soy.js',
							requires: [
								'soyutils'
							]
						}
					},
					root: MODULE_PATH + '/'
				}
			}
		}
	);
})();