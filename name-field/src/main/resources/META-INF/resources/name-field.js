AUI.add(
	'name-field',
	function(A) {
		var NameField = A.Component.create(
			{
				ATTRS: {
					username:{}
				},

				EXTENDS: Liferay.DDM.Renderer.Field,

				NAME: 'name-field',

				prototype: {
					getTemplateContext: function() {
						var instance = this;

						return A.merge(
							NameField.superclass.getTemplateContext.apply(instance, arguments),
							{
								username: fetchUsername(instance)
							}
						);
					}
				}

			}
		);

		Liferay.namespace('DDM.Field').NameField = NameField;
	},
	'',
	{
		requires: ['liferay-ddm-form-renderer-field']
	}
);

function fetchUsername(instance) {
	return instance.get('container') == undefined ? '' : (instance.getInputNode() != null ? instance.getValue() : '');
}